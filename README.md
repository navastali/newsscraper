# NewsScrapper

## How to Run
This application is packaged as a jar. No Tomcat or JBoss installation is necessary. You run it using the `java -jar` command.

- Clone this repository
- Make sure you are using JDK 1.8 and Maven 3.x
- You can build the project and run the tests by running `mvn clean package`

Once successfully built, you can run the service by one of these two methods:


```
        java -jar target/NewsScraper-1.0.0.jar
or
        mvn spring-boot:run 
```

Application URL: http://localhost:8088/NewsScraper/

Use Postman or any other REST client to access the webservice

API to get all authors (GET): http://localhost:8088/NewsScraper/api/author/

Default port number is ` 8088 `. Port number can be updated in the `application.yml` file

## Libraries
** Jsoup **

`jsoup` is a Java library for working with real-world HTML. It provides a very convenient API for extracting and manipulating data, using the best of DOM, CSS, and jquery-like methods.

Reference: https://jsoup.org/
