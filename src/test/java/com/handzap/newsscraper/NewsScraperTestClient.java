package com.handzap.newsscraper;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.web.client.RestTemplate;

public class NewsScraperTestClient {

	public static final String REST_SERVICE_URI = "http://localhost:8088/NewsScraper/api";

	/* GET */
	@SuppressWarnings("unchecked")
	private static void listAllAuthors() {
		System.out.println("Testing listAllAuthors API-----------");

		RestTemplate restTemplate = new RestTemplate();
		List<LinkedHashMap<String, Object>> authorMap = restTemplate
				.getForObject(REST_SERVICE_URI + "/author/", List.class);

		if (authorMap != null) {
			for (LinkedHashMap<String, Object> map : authorMap) {
				System.out.println("Author : Name=" + map.get("name")
						+ ", PetName=" + map.get("petname"));
			}
		} else {
			System.out.println("No author exist----------");
		}
	}

	public static void main(String args[]) {
		listAllAuthors();
	}
}