package com.handzap.newsscraper.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.handzap.newsscraper.model.Article;
import com.handzap.newsscraper.model.Author;
import com.handzap.newsscraper.service.NewsService;
import com.handzap.newsscraper.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class RestApiController {

	public static final Logger logger = LoggerFactory
			.getLogger(RestApiController.class);

	@Autowired
	NewsService userService; // Service which will do all data
								// retrieval/manipulation work

	// -------------------Retrieve All
	// Users---------------------------------------------

	@RequestMapping(value = "/author/", method = RequestMethod.GET)
	public ResponseEntity<List<Author>> listAllAuthors() {
		List<Author> users = userService.findAllAuthors();
		if (users.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Author>>(users, HttpStatus.OK);
	}

	// -------------------Retrieve Single
	// User------------------------------------------

	@RequestMapping(value = "/article/{author}", method = RequestMethod.GET)
	public ResponseEntity<List<Article>> getArticle(
			@PathVariable("author") String author) {
		logger.info("Fetching User with author {}", author);
		List<Article> articles = userService.findArticleByAuthor(author);
		if (articles == null) {
			logger.error("User with author {} not found.", author);
			return new ResponseEntity(new CustomErrorType("User with id "
					+ author + " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Article>>(articles, HttpStatus.OK);
	}

}