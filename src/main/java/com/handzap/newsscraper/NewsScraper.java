package com.handzap.newsscraper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.handzap.newsscraper" })
// same as @Configuration @EnableAutoConfiguration @ComponentScan combined
public class NewsScraper {

	public static void main(String[] args) {
		SpringApplication.run(NewsScraper.class, args);
	}
}
