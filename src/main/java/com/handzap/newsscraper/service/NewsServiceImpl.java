package com.handzap.newsscraper.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.handzap.newsscraper.model.Article;
import com.handzap.newsscraper.model.Author;
import com.handzap.newsscraper.repository.NewsRepository;

@Service("newsService")
public class NewsServiceImpl implements NewsService {

	@Autowired
	private NewsRepository newsRepository;

	private static final AtomicLong counter = new AtomicLong();

	public List<Author> findAllAuthors() {
		return newsRepository.getAuthors();
	}

	@Override
	public List<Article> findArticleByAuthor(String author) {
		// TODO Auto-generated method stub
		return null;
	}

}
