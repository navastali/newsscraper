package com.handzap.newsscraper.service;

import java.util.List;

import com.handzap.newsscraper.model.Article;
import com.handzap.newsscraper.model.Author;

public interface NewsService {

	List<Author> findAllAuthors();

	List<Article> findArticleByAuthor(String author);

}
