package com.handzap.newsscraper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.handzap.newsscraper.model.Article;
import com.handzap.newsscraper.model.Author;
import com.handzap.newsscraper.repository.NewsRepository;

@Component
public class Initializer implements CommandLineRunner {

	private static final Logger log = LoggerFactory
			.getLogger(Initializer.class);

	// @Autowired
	// private ScraperService scraperService;

	private final NewsRepository newsRepository;

	public Initializer(NewsRepository newsRepository) {
		this.newsRepository = newsRepository;
	}

	// @Override
	public void run(String... arg0) throws Exception {
		Document doc = Jsoup.connect(
				"https://www.thehindu.com/archive/web/2018/01/01/").get();
		log.debug(doc.title());
		Elements newsHeadlines = doc.select(".tpaper-container section");
		for (Element headline : newsHeadlines) {
			Elements archiveList = headline.getElementsByClass("archive-list");
			Elements articleUrls = archiveList.get(0).getElementsByTag("a");
			for (Element url : articleUrls) {
				try {
					doc = Jsoup.connect(url.absUrl("href")).get();
					String title = doc.getElementsByClass("title").text();
					String authorName = doc.getElementsByClass("auth-nm")
							.text();
					String description = null;
					Author author = new Author(authorName);
					Article article = new Article(title, description, author);
					newsRepository.addArticles(article);
				} catch (Exception e) {
					log.error("Error while reading article", e);
				}
			}
			// test.get
			// log.debug("%s\n\t%s", headline.attr("title"),
			// headline.absUrl("href"));
		}

	}
}
