package com.handzap.newsscraper.repository;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.handzap.newsscraper.model.Article;
import com.handzap.newsscraper.model.Author;

@Component
public class NewsRepositoryImpl implements NewsRepository {

	private static Hashtable<Author, List<Article>> articles = new Hashtable<Author, List<Article>>();

	@Override
	public void addArticles(Article article) {
		List<Article> lstArticles = articles.get(article.getAuthor());
		if (null != lstArticles) {
			lstArticles.add(article);
		} else {
			lstArticles = new ArrayList<Article>();
			lstArticles.add(article);
			articles.put(article.getAuthor(), lstArticles);
		}
	}

	@Override
	public List<Author> getAuthors() {
		Set<Author> keys = articles.keySet();
		List<Author> authors = new ArrayList<Author>(keys);
		return authors;
	}
}
