package com.handzap.newsscraper.repository;

import java.util.List;

import com.handzap.newsscraper.model.Article;
import com.handzap.newsscraper.model.Author;

public interface NewsRepository {

	void addArticles(Article article);

	List<Author> getAuthors();

}
